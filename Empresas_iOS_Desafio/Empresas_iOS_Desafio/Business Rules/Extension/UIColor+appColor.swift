//
//  UIColor+appColor.swift
//  Empresas_iOS_Desafio
//
//  Created by Joao Batista on 28/02/20.
//  Copyright © 2020 Joao Batista. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    static let charcoalGrey = UIColor(named: "charcoal_grey")
    static let charcoalGreyLight = UIColor(named: "charcoal_grey_light")
    static let darkRed = UIColor(named: "dark_red")
    static let eggShell = UIColor(named: "egg_shell")
    static let greenyBlue = UIColor(named: "greeny_blue")
    static let mediumPink = UIColor(named: "medium_pink")
    static let nightBlue = UIColor(named: "night_blue")
    static let red_app = UIColor(named: "red_app")
    static let disable = UIColor(named: "disable")
    static let warmGrey = UIColor(named: "warm_grey")
    static let enterpriseName = UIColor(named: "enterprise_name")
}
