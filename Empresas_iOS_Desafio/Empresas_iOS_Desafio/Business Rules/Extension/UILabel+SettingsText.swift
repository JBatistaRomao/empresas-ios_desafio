//
//  UILabel+SettingsText.swift
//  Empresas_iOS_Desafio
//
//  Created by Joao Batista on 29/02/20.
//  Copyright © 2020 Joao Batista. All rights reserved.
//

import Foundation
import UIKit
extension UILabel {
    func settingViewCodable(theContentMode mode: NSTextAlignment, numberLines lines: Int, andColor color: UIColor) {
        self.numberOfLines = lines
        self.textAlignment = mode
        self.textColor = color
        self.viewCodeMaskConstraints()
    }
}
