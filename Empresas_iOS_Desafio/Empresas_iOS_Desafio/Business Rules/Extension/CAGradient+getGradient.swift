//
//  CAGradient+getGradient.swift
//  Empresas_iOS_Desafio
//
//  Created by Joao Batista on 28/02/20.
//  Copyright © 2020 Joao Batista. All rights reserved.
//

import Foundation
import UIKit
extension CAGradientLayer {
    
   static func getGradient(view: UIView) -> CAGradientLayer {
        let gradient  = CAGradientLayer()
        let pink = UIColor.mediumPink!
        let blue = UIColor.nightBlue!
    gradient.colors = [pink.cgColor, blue.cgColor]
        gradient.startPoint = CGPoint(x: 0.5, y: 0.0)
        gradient.endPoint = CGPoint(x: 0.5, y: 6.0)
        gradient.frame = view.bounds
        return gradient
    }
}
