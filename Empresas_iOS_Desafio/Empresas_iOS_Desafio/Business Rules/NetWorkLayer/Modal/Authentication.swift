//
//  Authentication.swift
//  Empresas_iOS_Desafio
//
//  Created by Joao Batista on 01/03/20.
//  Copyright © 2020 Joao Batista. All rights reserved.
//

import Foundation

struct Authentication {
    static let headers = ["access-token", "client", "uid"]
    static let headersDefaultsKey = "authenticationHeaders"
}
