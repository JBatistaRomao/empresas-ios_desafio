//
//  NetworkService.swift
//  Empresas_iOS_Desafio
//
//  Created by Joao Batista on 28/02/20.
//  Copyright © 2020 Joao Batista. All rights reserved.
//

import Foundation
enum NetworkService<T: Codable>: ServiceProtocol {
    case login(User)
    case enterpriseIndex(Int, String)
    var baseURL: URL {
        let url = "https://empresas.ioasys.com.br/api/v1"
        
        return URL(string: url) ?? URL(fileURLWithPath: "")
    }
    var path: String {
        switch self {
        case .login:
            return "/users/auth/sign_in"
            
        case .enterpriseIndex(_, _):
            return "/enterprises"
        }
    }
    var method: HTTPMethod {
        switch self {
        case .login:
            return .post
        case .enterpriseIndex(_, _):
            return .get
        }
    }
    var task: HTTPTask {
        switch self {
        case .login(let user):
            let parameters: Parameters = ["email": user.email, "password": user.password]
            return .requestParameters(parameters)
        case .enterpriseIndex(let code, let name):
            let parameters: Parameters = ["enterprise_types": "\(code)", "name": name]
            return .requestParameters(parameters)
        }
    }
    
    var headers: Headers? {
        switch self {
        case .login:
            return ["Content-type": "application/json"]
        case .enterpriseIndex(_, _):
            var header: [String:String] = ["Content-type": "application/json"]
            for headerName in Authentication.headers {
                let headerValue = UserDefaults.standard.dictionary(forKey: Authentication.headersDefaultsKey)?[headerName]
                if let headerValue = headerValue as? String {
                    header[headerName] = headerValue
                }
            }
            return header
            
        }
    }
    
    var parametersEncoding: ParametersEncoding {
        switch self {
        case .login:
            return .json
        case .enterpriseIndex(_, _):
            return .url
        }
    }
}
