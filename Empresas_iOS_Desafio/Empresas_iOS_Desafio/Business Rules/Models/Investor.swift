// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

// MARK: - InvestorDao
struct InvestorDAO: Codable {
    let investor: Investor
}

// MARK: - Investor
struct Investor: Codable {
    let id: Int
    let investorName, email, city, country: String
    let balance: Int
    let photo: String
    let firstAccess, superAngel: Bool

    enum CodingKeys: String, CodingKey {
        case id
        case investorName = "investor_name"
        case email, city, country, balance, photo
        case firstAccess = "first_access"
        case superAngel = "super_angel"
    }
}
