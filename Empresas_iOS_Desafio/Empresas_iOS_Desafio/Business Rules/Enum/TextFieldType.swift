//
//  TextFieldType.swift
//  Empresas_iOS_Desafio
//
//  Created by Joao Batista on 28/02/20.
//  Copyright © 2020 Joao Batista. All rights reserved.
//

import Foundation

enum TextFieldType: String {
    case email = "envelope"
    case password = "lock.open"
    
    var name: String {
        switch self {
        case .email:
            return "Email"
        case .password:
            return "Senha"
        }
    }
}
