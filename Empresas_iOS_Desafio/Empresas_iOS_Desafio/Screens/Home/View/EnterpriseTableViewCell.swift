//
//  EnterpriseTableViewCell.swift
//  Empresas_iOS_Desafio
//
//  Created by Joao Batista on 01/03/20.
//  Copyright © 2020 Joao Batista. All rights reserved.
//

import Foundation
import UIKit
class EnterpriseTableViewCell: UITableViewCell {
    
    let photo: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "enterprise_default")
        imageView.contentMode =  .scaleAspectFit
        imageView.viewCodeMaskConstraints()
        return imageView
    }()
    
    let name: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 17)
       
        label.textColor = UIColor.black
        label.viewCodeMaskConstraints()
        return label
    }()
    
    let business: UILabel = {
        let label = UILabel()
        label.font = UIFont.italicSystemFont(ofSize: 17)
      
         label.textColor = UIColor.warmGrey
        label.viewCodeMaskConstraints()
        return label
    }()
    
    let country: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 17)
        
         label.textColor = UIColor.warmGrey
        label.viewCodeMaskConstraints()
        return label
    }()
    
    let cardView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        view.makeRoundBorder(withCornerRadius: 8)
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
        self.backgroundColor = UIColor.eggShell
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupValues(enterprise: Enterpris) {
        name.text = enterprise.enterpriseName
        business.text = enterprise.enterpriseType.enterpriseTypeName
        country.text = enterprise.country
    }

}

extension EnterpriseTableViewCell: ViewCodable {
    func buildViewHierarchy() {
                
        addSubview(cardView)
        cardView.addSubview(photo)
        cardView.addSubview(name)
        cardView.addSubview(business)
        cardView.addSubview(country)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            cardView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            cardView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            cardView.topAnchor.constraint(equalTo: topAnchor, constant: 5),
            cardView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5)
        ])
        
        NSLayoutConstraint.activate([
            photo.topAnchor.constraint(equalTo: cardView.topAnchor, constant: 0),
            photo.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: 10),
            photo.bottomAnchor.constraint(equalTo: cardView.bottomAnchor, constant: 0),
            photo.widthAnchor.constraint(equalToConstant: 125)
        ])
        
        NSLayoutConstraint.activate([
            name.topAnchor.constraint(equalTo: photo.topAnchor, constant: 20),
            name.leadingAnchor.constraint(equalTo: photo.trailingAnchor, constant: 15),
            name.heightAnchor.constraint(equalToConstant: 20)
        ])
        NSLayoutConstraint.activate([
            business.topAnchor.constraint(equalTo: name.bottomAnchor, constant: 5),
            business.leadingAnchor.constraint(equalTo: photo.trailingAnchor, constant: 15),
            business.heightAnchor.constraint(equalToConstant: 20)
        ])
        NSLayoutConstraint.activate([
            country.topAnchor.constraint(equalTo: business.bottomAnchor, constant: 5),
            country.leadingAnchor.constraint(equalTo: photo.trailingAnchor, constant: 15),
            country.heightAnchor.constraint(equalToConstant: 20)
        ])
    }
    
    func setupAdditionalConfiguration() {
    }
}
