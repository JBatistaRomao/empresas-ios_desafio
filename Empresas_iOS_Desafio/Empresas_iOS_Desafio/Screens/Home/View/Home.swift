//
//  Home.swift
//  Empresas_iOS_Desafio
//
//  Created by Joao Batista on 01/03/20.
//  Copyright © 2020 Joao Batista. All rights reserved.
//

import Foundation
import UIKit
class Home: UIView {
    var activityView: UIActivityIndicatorView!
    
    let enterprises: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = UIColor.eggShell
        tableView.rowHeight = 130
        tableView.separatorStyle = .none
        tableView.viewCodeMaskConstraints()
        return tableView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension Home: ViewCodable {
    func buildViewHierarchy() {
        addSubview(enterprises)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            enterprises.topAnchor.constraint(equalTo: topAnchor, constant: 18),
            enterprises.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: 18),
            enterprises.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            enterprises.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16)
        ])
    }
    
    func setupAdditionalConfiguration() {
        self.activityView = UIActivityIndicatorView(style: .large)
        activityView.translatesAutoresizingMaskIntoConstraints = false
    }
}
// MARK: - Target Activity

extension Home {
    func setupActivityView() {
        let view = createViewForActivity()
        view.addSubview(activityView)
        activityView.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
        activityView.translatesAutoresizingMaskIntoConstraints = false
        activityView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        activityView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        activityView.color = UIColor.greenyBlue
    }
    
    private func createViewForActivity() -> UIView {
        let view = UIView(frame: UIScreen.main.bounds)
        self.addSubview(view)
        view.tag = 1234
        view.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)
        view.makeRoundBorder(withCornerRadius: 5)
        return view
        
    }
    func removeSubview(withTag tag: Int = 1234) {
        if let viewwithTag = self.viewWithTag(tag) {
            viewwithTag.removeFromSuperview()
        }
    }
}
