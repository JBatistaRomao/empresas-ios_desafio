//
//  HomeViewController.swift
//  Empresas_iOS_Desafio
//
//  Created by Joao Batista on 01/03/20.
//  Copyright © 2020 Joao Batista. All rights reserved.
//
import Foundation
import UIKit

class HomeViewController: UIViewController {
    
    private var alertError = Alert()
    let search = UISearchBar()
    var customView: Home!
    var tableDatasource: EnterpriseDataSource!
    private let provider = URLSessionProvider()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.eggShell
        setupNavigation()
        search.delegate = self
        setupTableView(with: [])
    }
    
    override func loadView() {
        customView = Home()
        view = customView
    }
    func setupTableView(with enterprises: [Enterpris]) {
        tableDatasource = EnterpriseDataSource(items: enterprises, tableView: self.customView.enterprises)
        tableDatasource.delegate = self
    }
    
}

// MARK: - Manipulation Navigation
extension HomeViewController {
    func setupNavigation() {
        setTitleViewImage()
        navigationController?.navigationBar.setGradient()
    }
    @objc func searchBarIcon() {
        navigationItem.titleView = search
        navigationItem.rightBarButtonItem = nil
        let textFieldInsideSearchBar = search.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.attributedPlaceholder = NSAttributedString(string: "Pesquisar", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        search.setShowsCancelButton(true, animated: true)
        
    }
    @objc func setTitleViewImage() {
        let imageView = UIImageView(image: UIImage(named: "ioasys_nav"))
        imageView.contentMode = .scaleAspectFill
        navigationItem.titleView = imageView
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(searchBarIcon))
    }
    
}

// MARK: - SearchBar Delegate
extension HomeViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        setTitleViewImage()
    }
    
    func searchBarSearchButtonClicked( _ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        
        customView.setupActivityView()
        customView.activityView.startAnimating()
        guard let text = searchBar.text else {return}
        provider.request(type: EnterpriseDAO.self, service: NetworkService<User>.enterpriseIndex(1, text)) { (response) in
            DispatchQueue.main.async {
                self.customView.activityView.stopAnimating()
                self.customView.removeSubview()
                switch response {
                case .success(let result):
                    self.tableDatasource.updateTable(items: result.enterprises)
                case .failure(let error):
                    print(error)
                    let alertView = self.alertError.errorNetworkUnknown()
                    self.present(alertView, animated: true, completion: nil)
                    
                }
                
            }
        }
    }
    
}
extension HomeViewController: EnterpriseDataSourceDelegate {
    func presentDetails(enterprise: Enterpris) {
        setTitleViewImage()
        let controller  = DetailsViewController(enterprise: enterprise)
        controller.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
