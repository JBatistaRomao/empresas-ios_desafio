//
//  EnterpriseDataSource.swift
//  Empresas_iOS_Desafio
//
//  Created by Joao Batista on 01/03/20.
//  Copyright © 2020 Joao Batista. All rights reserved.
//

import Foundation
import UIKit

protocol EnterpriseDataSourceDelegate: class {
    func presentDetails(enterprise: Enterpris)
}
class EnterpriseDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    var items: [Enterpris] = []
    var tableView: UITableView
    fileprivate let cellId = "id"
    let sharingIconClicked = UIImage(named: "sharingClicked")
    weak var delegate: EnterpriseDataSourceDelegate?
    
    required init(items: [Enterpris], tableView: UITableView) {
        self.items = items
        self.tableView = tableView
        super.init()
        tableView.register(EnterpriseTableViewCell.self, forCellReuseIdentifier: cellId)
        self.setupTableView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "id", for: indexPath) as? EnterpriseTableViewCell
        cell?.setupValues(enterprise: self.items[indexPath.row])
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.presentDetails(enterprise: items[indexPath.row])
    }
    
    func setupTableView() {
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
    
    func updateTable(items: [Enterpris]) {
        self.items = items
        tableView.reloadData()
    }
}
