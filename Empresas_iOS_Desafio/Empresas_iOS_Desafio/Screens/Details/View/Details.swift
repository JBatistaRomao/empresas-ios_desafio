//
//  Details.swift
//  Empresas_iOS_Desafio
//
//  Created by Joao Batista on 01/03/20.
//  Copyright © 2020 Joao Batista. All rights reserved.
//

import UIKit

class Details: UIView {
    
    let photo: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "details")
        imageView.contentMode = .scaleAspectFit
        imageView.viewCodeMaskConstraints()
        return imageView
    }()
    
    let enterprisDescription: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .left
        label.textColor = UIColor.warmGrey
        label.font = UIFont.systemFont(ofSize: 17)
        label.viewCodeMaskConstraints()
        return label
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        backgroundColor = UIColor.eggShell
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension Details: ViewCodable {
    func buildViewHierarchy() {
        addSubview(photo)
        addSubview(enterprisDescription)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            photo.topAnchor.constraint(equalTo: topAnchor, constant: 24),
            photo.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 22),
            photo.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -22),
            photo.heightAnchor.constraint(equalToConstant: 155)
        ])
        
        NSLayoutConstraint.activate([
            enterprisDescription.topAnchor.constraint(equalTo: photo.bottomAnchor, constant: 19),
            enterprisDescription.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 22),
            enterprisDescription.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -22),
            enterprisDescription.bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor, constant: 0)
        ])
    }
    
    func setupAdditionalConfiguration() {
    }
}
