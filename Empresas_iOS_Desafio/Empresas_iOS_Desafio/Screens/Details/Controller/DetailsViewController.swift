//
//  DetailsViewController.swift
//  Empresas_iOS_Desafio
//
//  Created by Joao Batista on 01/03/20.
//  Copyright © 2020 Joao Batista. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    let enterprise: Enterpris
    let customView : Details
    init(enterprise: Enterpris) {
        self.enterprise = enterprise
        self.customView = Details()
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) {
           fatalError("init(coder:) has not been implemented")
    }
       
    override func viewDidLoad() {
        super.viewDidLoad()
        title = enterprise.enterpriseName
        customView.enterprisDescription.text = enterprise.enterprisDescription
        
        // Do any additional setup after loading the view.
    }
    override func loadView() {
        view = customView
    }
   
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
