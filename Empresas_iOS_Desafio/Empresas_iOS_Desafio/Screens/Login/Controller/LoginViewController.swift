//
//  LoginViewController.swift
//  Empresas_iOS_Desafio
//
//  Created by Joao Batista on 27/02/20.
//  Copyright © 2020 Joao Batista. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    private var alertError = Alert()
    private var customView = Login(frame: .zero)
    private let provider = URLSessionProvider()
    private var failedLogin = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSettings()
    }
    
    override func loadView() {
        view = customView
    }
    
    func initialSettings() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        setupTarget()
        
    }
    
    
    func setupTarget() {
        customView.login.addTarget(self, action: #selector(login(sender:)), for: .touchUpInside)
        customView.email.textField.addTarget(self, action: #selector(tapInTextField(sender:)), for: .editingDidBegin)
        customView.password.textField.addTarget(self, action: #selector(tapInTextField(sender:)), for: .editingDidBegin)
        let tapTochangeVisiblePassWord = UITapGestureRecognizer(target: self, action: #selector(changeVisiblePassWord(sende:)))
        customView.password.iconRight.addGestureRecognizer(tapTochangeVisiblePassWord)
    }
    
    private func login(email: String, password: String) {
        customView.setupActivityView()
        customView.activityView.startAnimating()
        
        let user = User(email: email, password: password)
        provider.request(type: InvestorDAO.self, service: NetworkService<User>.login(user)) { (response) in
            DispatchQueue.main.async {
                self.customView.activityView.stopAnimating()
                self.customView.removeSubview()
                switch response {
                case .success(_):
                    self.presenteHomeViewController()
                case .failure(let error):
                    self.showAlertErro(error: error)
                    
                }
                
            }
        }
    }
    
    private func presenteHomeViewController() {
        let controller  = HomeViewController()
        let navigation = UINavigationController()
        navigation.viewControllers = [controller]
        navigation.modalPresentationStyle = .fullScreen
        self.present(navigation, animated: true, completion: nil)
    }
    
    private func errorLogin() {
        customView.email.setupAlert(size: 20)
        customView.password.setupAlert(size: 20)
        customView.setupAlert()
        customView.password.iconRight.isUserInteractionEnabled = false
        
    }
    
    private func showAlertErro(error: NetworkError) {
        switch error {
        case .clientError(statusCode: 401, dataResponse: _):
            errorLogin()
            failedLogin = true
        default:
            let alertView = alertError.errorNetworkUnknown()
            present(alertView, animated: true, completion: nil)
        }
    }
}

// MARK: - Target Action
extension LoginViewController {
    @objc func login(sender: UIButton) {
        sender.blink()
        customView.email.removeKeyboard()
        customView.password.removeKeyboard()
        guard let email = customView.email.textField.text?.lowercased(), let password = customView.password.textField.text else {
            errorLogin()
            return
        }
        login(email: email, password: password)
    }
    
    @objc func tapInTextField(sender: UITextField) {
        if failedLogin == true {
            customView.email.removeAlert(size: 0)
            customView.password.removeAlert(size: 20)
            customView.removeAlert()
            customView.password.iconRight.isUserInteractionEnabled = true
        }
    }
    
    @objc func changeVisiblePassWord(sende: NSNotification) {
        if customView.password.textField.isSecureTextEntry == true {
            customView.password.visiblePassWord()
        } else {
            customView.password.notVisiblePassWord()
        }
    }
}

// MARK: - Move view to insert keyboard
extension LoginViewController {
    @objc func keyboardWillShow(notification: NSNotification) {
        if customView.frame.origin.y == 0 {
            customView.frame.origin.y -= getSizeMoveKeyboard()
        }
    }
    private func getSizeMoveKeyboard () -> CGFloat {
        let centerWelcome = customView.welcome.center.y
        let halfHeightWelcome = customView.welcome.frame.height / 2
        let pedding: CGFloat =  45.0
        let move = centerWelcome - halfHeightWelcome - pedding
        return move
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        if customView.frame.origin.y != 0 {
            customView.frame.origin.y = 0
        }
    }
}
