//
//  Login.swift
//  Empresas_iOS_Desafio
//
//  Created by Joao Batista on 27/02/20.
//  Copyright © 2020 Joao Batista. All rights reserved.
//

import UIKit

class Login: UIView {
    
    private var heightErrorLogin: NSLayoutConstraint!
    var activityView: UIActivityIndicatorView!
    
    let errorAlert: UILabel = {
        let label = UILabel()
        label.text = "Credenciais informadas são inválidas, tente novamente."
        label.font = UIFont.systemFont(ofSize: 10)
        label.settingViewCodable(theContentMode: .center, numberLines: 0, andColor: UIColor.red_app ?? UIColor.black)
        return label
    }()
    
    let logo: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "ioasys_Login"))
        imageView.contentMode = .scaleAspectFit
        imageView.viewCodeMaskConstraints()
        return imageView
    }()
    
    let welcome: UILabel = {
        let label = UILabel()
        label.text = "BEM-VINDO AO \n EMPRESAS"
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.settingViewCodable(theContentMode: .center, numberLines: 0, andColor: UIColor.charcoalGrey ?? UIColor.black)
        return label
    }()
    
    let about: UILabel = {
        let label = UILabel()
        label.text = "Com o Empresas você busca pelas empresas e descobre mais sobre elas"
        label.font = UIFont.systemFont(ofSize: 16)
        label.settingViewCodable(theContentMode: .center, numberLines: 0, andColor: UIColor.charcoalGrey ?? UIColor.black)
        return label
    }()
    
    let cardView: UIView = {
        let view = UIView()
        view.viewCodeMaskConstraints()
        return view
    }()
    let login: UIButton = {
        let button = UIButton()
        button.setTitle("ENTRAR", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        button.backgroundColor = UIColor.greenyBlue
        button.viewCodeMaskConstraints()
        button.makeRoundBorder(withCornerRadius: 3)
        return button
    }()
    
    let email = CustomTextField(withType: .email, andSizeIconRight: 0)
    let password = CustomTextField(withType: .password, andSizeIconRight: 25)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.eggShell
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension Login: ViewCodable {
    func buildViewHierarchy() {
        addSubview(cardView)
        addSubview(logo)
        addSubview(welcome)
        addSubview(about)
        cardView.addSubview(errorAlert)
        cardView.addSubview(email)
        cardView.addSubview(password)
        cardView.addSubview(login)
    }
    
    func setupConstraints() {
        let marginCard: CGFloat = 10
        let margins: CGFloat = 30.0
        
        NSLayoutConstraint.activate([
            logo.topAnchor.constraint(equalTo: topAnchor, constant: 70),
            logo.centerXAnchor.constraint(equalTo: centerXAnchor),
            logo.heightAnchor.constraint(equalToConstant: 45),
            logo.widthAnchor.constraint(equalToConstant: 184)
        ])
        
        NSLayoutConstraint.activate([
            welcome.topAnchor.constraint(equalTo: logo.bottomAnchor, constant: 48),
            welcome.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0)
        ])
        
        NSLayoutConstraint.activate([
            about.topAnchor.constraint(equalTo: welcome.bottomAnchor, constant: 17),
            about.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 50),
            about.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -50)
        ])
        
        NSLayoutConstraint.activate([
            cardView.topAnchor.constraint(equalTo: about.bottomAnchor, constant: 80),
            cardView.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.height / 4),
            cardView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: margins),
            cardView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -margins),
            cardView.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            login.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: marginCard),
            login.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -marginCard),
            login.heightAnchor.constraint(equalToConstant: 50),
            login.bottomAnchor.constraint(equalTo: cardView.bottomAnchor, constant: 0)
        ])
        
        heightErrorLogin = errorAlert.heightAnchor.constraint(equalToConstant: 0)
        
        NSLayoutConstraint.activate([
            errorAlert.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: marginCard),
            errorAlert.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -marginCard),
            errorAlert.bottomAnchor.constraint(equalTo: login.topAnchor, constant: -22),
            heightErrorLogin
        ])
        NSLayoutConstraint.activate([
            password.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: marginCard),
            password.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -marginCard),
            password.heightAnchor.constraint(equalToConstant: 30),
            password.bottomAnchor.constraint(equalTo: errorAlert.topAnchor, constant: -12)
        ])
        NSLayoutConstraint.activate([
            email.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: marginCard),
            email.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -marginCard),
            email.heightAnchor.constraint(equalToConstant: 30),
            email.bottomAnchor.constraint(equalTo: password.topAnchor, constant: -20)
        ])
    }
    
    func setupAdditionalConfiguration() {
        self.activityView = UIActivityIndicatorView(style: .large)
        activityView.translatesAutoresizingMaskIntoConstraints = false
    }
    
}

extension Login {
    
    func setupAlert(size: CGFloat = 15) {
        heightErrorLogin.constant = size
        login.isEnabled = false
        login.backgroundColor = UIColor.disable
        self.layoutIfNeeded()
        
    }
    
    func removeAlert(size: CGFloat = 0) {
        heightErrorLogin.constant = size
        login.isEnabled = true
        login.backgroundColor = UIColor.greenyBlue
        self.layoutIfNeeded()
    }
    
    func setupActivityView() {
        let view = createViewForActivity()
        view.addSubview(activityView)
        activityView.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
        activityView.translatesAutoresizingMaskIntoConstraints = false
        activityView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        activityView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        activityView.color = UIColor.greenyBlue
    }
    
    private func createViewForActivity() -> UIView {
        let view = UIView(frame: self.frame)
        self.addSubview(view)
        view.tag = 1234
        view.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)
        view.makeRoundBorder(withCornerRadius: 5)
        return view
        
    }
    func removeSubview(withTag tag: Int = 1234) {
        if let viewwithTag = self.viewWithTag(tag) {
            viewwithTag.removeFromSuperview()
        }
    }
}
