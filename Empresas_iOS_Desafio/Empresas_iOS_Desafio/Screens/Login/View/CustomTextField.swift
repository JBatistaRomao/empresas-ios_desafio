//
//  CustomTextField.swift
//  Empresas_iOS_Desafio
//
//  Created by Joao Batista on 27/02/20.
//  Copyright © 2020 Joao Batista. All rights reserved.
//

import Foundation
import UIKit

class CustomTextField: UIView {
    
    private let size: CGFloat
    private var heightConstraint: NSLayoutConstraint!
    private var widthConstrainnt: NSLayoutConstraint!
    private var peddingLine: CGFloat  = 3
    private var type: TextFieldType!
    
    let textField: UITextField = {
        let textField = UITextField()
        textField.viewCodeMaskConstraints()
        textField.textColor = UIColor.charcoalGrey
        return textField
    }()
    
    let iconRight: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = UIColor.greenyBlue
        imageView.viewCodeMaskConstraints()
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    private let iconLeft: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = UIColor.mediumPink
        imageView.viewCodeMaskConstraints()
        return imageView
    }()
    
    private let line: UIView = {
        let view = UIView()
        view.viewCodeMaskConstraints()
        view.backgroundColor = UIColor.charcoalGrey
        return view
    }()
    
    init(withType type: TextFieldType, andSizeIconRight size: CGFloat) {
        self.size = size
        self.type = type
        super.init(frame: .zero)
        setup(theType: type)
        setupView()
        self.viewCodeMaskConstraints()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension CustomTextField: ViewCodable {
    func buildViewHierarchy() {
        addSubview(line)
        addSubview(textField)
        addSubview(iconLeft)
        addSubview(iconRight)
    }
    
    func setupConstraints() {
        
        heightConstraint = iconRight.heightAnchor.constraint(equalToConstant: size)
        widthConstrainnt = iconRight.widthAnchor.constraint(equalToConstant: size)
        
        NSLayoutConstraint.activate([
            iconRight.centerYAnchor.constraint(equalTo: textField.centerYAnchor),
            iconRight.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            heightConstraint,
            widthConstrainnt
        ])
        
        NSLayoutConstraint.activate([
            line.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0),
            line.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            line.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            line.heightAnchor.constraint(equalToConstant: 0.3)
        ])
        
        NSLayoutConstraint.activate([
            iconLeft.centerYAnchor.constraint(equalTo: textField.centerYAnchor),
            iconLeft.leadingAnchor.constraint(equalTo: leadingAnchor, constant: peddingLine),
            iconLeft.widthAnchor.constraint(equalToConstant: 20),
            iconLeft.heightAnchor.constraint(equalToConstant: 20)
            
        ])
        
        NSLayoutConstraint.activate([
            textField.bottomAnchor.constraint(equalTo: line.topAnchor, constant: -peddingLine),
            textField.trailingAnchor.constraint(equalTo: iconRight.leadingAnchor, constant: -peddingLine),
            textField.topAnchor.constraint(equalTo: topAnchor, constant: 0),
            textField.leadingAnchor.constraint(equalTo: iconLeft.trailingAnchor, constant: peddingLine*2)
        ])
    }
    
    func setupAdditionalConfiguration() {
    }
    
}

extension CustomTextField {
    func setup(theType type: TextFieldType) {
        switch type {
        case .email:
            setValues(theType: type)
            textField.keyboardType = .emailAddress
        case .password :
            setValues(theType: type)
            textField.isSecureTextEntry = true
            iconRight.image = UIImage(systemName: "eye")
            
        }
    }
    
    private func setValues(theType type: TextFieldType) {
        iconLeft.image = UIImage(systemName: type.rawValue)
        textField.attributedPlaceholder = NSAttributedString(string: type.name, attributes: [NSAttributedString.Key.foregroundColor: UIColor.charcoalGreyLight ?? UIColor.systemGray5])
        
    }
    
    func visiblePassWord(andChangeIcon: Bool = true) {
        switch type {
        case .password:
            textField.isSecureTextEntry = false
            if andChangeIcon {
                iconRight.image = UIImage(systemName: "eye.fill")
            }
            
        default:
            break
        }
    }
    
    func notVisiblePassWord(andChangeIcon: Bool = true) {
        switch type {
        case .password:
            textField.isSecureTextEntry = true
            if andChangeIcon {
                iconRight.image = UIImage(systemName: "eye")
            }
        default:
            break
        }
    }
    
    func setupAlert(size: CGFloat) {
        heightConstraint.constant = size
        widthConstrainnt.constant = size
        iconRight.layoutIfNeeded()
        chageIcon(withName: "exclamationmark.circle.fill", anColor: UIColor.red_app ?? UIColor.red)
        line.backgroundColor = UIColor.red_app
        
    }
    
    func chageIcon(withName name: String, anColor color: UIColor = UIColor.red_app ?? UIColor.red) {
        iconRight.image = UIImage(systemName: name )
        iconRight.tintColor = color
    }
    
    func removeAlert(size: CGFloat) {
        heightConstraint.constant = size
        widthConstrainnt.constant = size
        iconRight.layoutIfNeeded()
        setup(theType: type)
        line.backgroundColor = UIColor.charcoalGrey
        iconRight.tintColor = UIColor.greenyBlue
        
    }
    
    func removeKeyboard() {
        textField.resignFirstResponder()
    }
    
}
